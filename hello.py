from flask import Flask, render_template

app = Flask(__name__)

@app.route("/")
def index():
    message="Ceci est un message de Flask!"
    fruits = ["Pomme", "Banane", "Orange", "Fraise", "Raisin", "Ananas"]
    return render_template("present.html", message = message, fruits = fruits)   

if __name__ == '__main__':
    app.run(debug=True)